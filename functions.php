<?php

// Include composer dependencies
include_once __DIR__ . '/vendor/autoload.php';

// Functions
function getIndex($params) {
    $content = file_get_contents("index/index.json");
    $content = utf8_encode($content);
    return json_decode($content, true);
}
