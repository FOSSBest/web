<?php

// Include composer dependencies
include_once __DIR__ . '/vendor/autoload.php';
include 'functions.php';

// Init router
$router = new AltoRouter();

// Router mapping
$router->map('GET', '/', function() {}, 'home');

// Router matching
$match = $router->match();

// Template engine settings and variables
$options = [
    'paths' => [
        'views/',
    ],
    'enable_profiler' => false,
    'profiler' => [
        'time_precision' => 3,
        'line_height'    => 30,
        'display'        => true,
        'log'            => false,
    ],
];
$variables = [];

// Home view
if ($match['name'] == 'home') {
    $variables['title'] = 'FOSSBest';
    $variables['index'] = getIndex($variables['params']);
}

// Render the appropriate route
if(is_array($match) && is_callable($match['target'])) {
    switch ($match['name']) {
        case 'home':
            Phug::displayFile('index', $variables, $options);
            break;
    }
} else {
    // No route was matched
    Phug::displayFile('404', $variables, $options);
}
